import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GoBackButton from "../components/GoBackButton";
import { getBack } from "../actions/router";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return {};
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ getBack }, dispatch)
  };
};

/** Finaly we connect the component with the store */
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GoBackButton);
