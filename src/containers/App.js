import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import App from "../App";

/** We map states to props to turn them into read-only mode */
const mapStateToProps = state => {
  return {
    currentRouteName: state.router.history[state.router.history.length - 1]
  };
};

// We map action creators to props to access them through container
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({}, dispatch)
  };
};

/** Finaly we connect the component with the store */
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
