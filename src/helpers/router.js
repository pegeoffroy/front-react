import React from "react";
import HomeScreen from "../containers/HomeScreen";

export const renderCurrentRoute = routeName => {
  switch (routeName) {
    case "home":
      return <HomeScreen />;
    default:
      return <HomeScreen />;
  }
};
