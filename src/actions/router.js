import types from "../constants/actionTypes";

export const pushRoute = routeName => {
  return { type: types.PUSH_ROUTE, routeName };
};

export const getBack = () => {
  return { type: types.GET_BACK };
};

export const backHome = () => {
  return { type: types.CLEAR_HISTORY };
};
