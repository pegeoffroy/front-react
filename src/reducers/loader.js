import types from "../constants/actionTypes";

let initialState = {
  isLoading: false
};

export const loader = (state = initialState, action) => {
  switch (action.type) {
    case types.SHOW_LOADER:
      return {
        isLoading: true
      };
    case types.HIDE_LOADER:
      return {
        isLoading: false
      };
    default:
      return state;
  }
};
