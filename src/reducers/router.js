import types from "../constants/actionTypes";

let initialState = {
  history: []
};

export const router = (state = initialState, action) => {
  switch (action.type) {
    case types.PUSH_ROUTE:
      return {
        ...state,
        history: [...state.history, action.routeName]
      };
    case types.GET_BACK:
      const updatedHistory = removeLastItem(state.history);
      return {
        ...state,
        history: updatedHistory
      };
    case types.CLEAR_HISTORY:
      return {
        history: ["home"]
      };
    default:
      return state;
  }
};

const removeLastItem = array => {
  return array.slice(0, array.length - 1);
};
