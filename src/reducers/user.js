import types from "../constants/actionTypes";

let initialState = {
  email: ""
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_USER_EMAIL:
      return {
        email: action.email
      };
    default:
      return state;
  }
};
