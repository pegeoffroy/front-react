import React from "react";
import Button from "./Button";

const LangSelect = (props) => {
  const { source, alt, label, onClick, path } = props;
  return (
    <div className="lang-wrapper">
      <img className="lang-logo" src={source} alt={alt} />
      <Button label={label} onClick={() => onClick(path)} />
    </div>
  );
}

export default LangSelect;