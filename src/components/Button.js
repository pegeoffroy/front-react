import React from "react";

const Button = (props) => {
  const { label, onClick, isGoBack } = props;
  return (
    <button
      className={
        isGoBack
          ? "gobackbutton-color waves-effect waves-light btn"
          : "waves-effect waves-light btn"
      }
      onClick={onClick}
    >
      {label}
    </button>
  );
}
export default Button

