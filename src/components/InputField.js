import React from "react";

const InputField = (props) => {
  const { name, label, onChange } = props;
  return (
    <div className="input-field">
      <label htmlFor={name}>{label}</label>
      <input name={name} onChange={event => onChange(event)} type="text" />
    </div>
  );
}

export default InputField;